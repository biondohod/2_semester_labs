﻿#include <iostream>
#include <chrono>
#pragma comment(linker, "/STACK:40000000")
const int N = 1000000;
class Timer
{
private:
	// Псевдонимы типов используются для удобного доступа к вложенным типам
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};
void BubbleSort(int* arr)
{
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}

}
void QuickSort(int first, int last, int* arr)
{
	if (first < last)
	{
		int left = first;
		int right = last;
		//int medium = rand() % N;
		int medium = (first + last) / 2;
		int k = arr[medium];
		do
		{
			while (arr[left] < k)
				left++;
			while (arr[right] > k)
				right--;
			if (left <= right)
			{
				std::swap(arr[left], arr[right]);
				left++;
				right--;
			}
		} while (left < right);
		QuickSort(first, right, arr);
		QuickSort(left, last, arr);
	}
}
/*void Print(int* arr)
{
	std::cout << "[ ";
	for (int i = 0; i < N; i++)
		std::cout << arr[i] << " ";
	std::cout << "]" << std::endl;

}*/

int main()
{
	Timer t;
	int arr[N];
	int first = 0;
	int last = N - 1;
	for (int i = 0; i < N; i++)
		arr[i] = rand();
	//QuickSort(first, last, arr);
	BubbleSort(arr);
	std::cout << "Time elapsed: " << t.elapsed();
	return 0;

}
