#include <iostream>
#include <chrono>

const int M = 1000;

class Timer
{
private:
	// ���������� ����� ������������ ��� �������� ������� � ��������� �����
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};


struct List
{
	List* next;
	int num;
};

void List_Add(List* head, int num)
{
	List* p = new List;
	p->num = num;

	p->next = head->next;
	head->next = p;
}

int List_Max(List* head) {
	if (head->next != nullptr) {
		int maxnum = INT_MIN;
		List* p = head->next;
		while (p != nullptr)
		{
			if (p->num > maxnum)
				maxnum = p->num;
			p = p->next;
		}
		return(maxnum);
	}
	else {
		std::cout << "Error! Empty list" << std::endl;
		return(-1);
	}
		
}

void List_SwapSort(List* head)
{
	List* p = head->next;
	while (p->next->next != nullptr)
	{
		List* q = p->next;
		while (q->next != nullptr)
		{
			if (p->num < q->num)
				std::swap(p->num, q->num);
			q = q->next;
		}
		p = p->next;
	}
}



void List_Delete(List* head) {
	List* tmp;
	List* p = head;
	while (p->next != nullptr)
	{
		if (p->next->num == List_Max(head) )
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
			break;
		}
		else
			p = p->next;
	}

}

void List_Delete_Max(List* head)
{
	List* tmp;
	List* p = head;
	tmp = p->next;
	p->next = p->next->next;
	delete tmp;
}


void List_Clear(List* head)
{
	List* tmp;
	List* p = head->next;
	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}

void List_Print(List* head)
{
	List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->num << std::endl;
		p = p->next;
	}
}

void Array_SwapSort(int* arr, int N) {
	for (int i = 1; i < N; i++) {
		if (arr[i] < arr[i - 1])
			std::swap(arr[i], arr[i - 1]);
	}
}

void Array_Delete_Max(int& N) {
	N--;
}



int main() {
	List* head = new List;
	head->next = nullptr;
	/* //�������� ������������� �������� ��� ���������� ������
	for (int i = 0; i < 10; i++) {
		List_Add(head, rand());
	}
	Timer t;
	for (int i = 0; i < M; i++) {
		List_Delete(head);
	} */
	/*//�������� ������������� �������� � ����������� �������
	for (int i = 0; i < N; i++) {
		List_Add(head, rand());
	}
	Timer t;
	List_SwapSort(head);
	for (int i = 0; i < M; i++) {
		List_Delete_Max(head);
	}
	std::cout << "Time elapsed: " << t.elapsed();*/
	//�������� ������������� �������� � �������
	int N = 10000;
	int* arr = new int[N];
	for (int i = 0; i < N; i++) {
		arr[i]=rand();
	}
	Timer t;
	Array_SwapSort(arr, N);
	for (int i = 0; i < M; i++) {
		Array_Delete_Max(N);
	}
	std::cout << "Time elapsed: " << t.elapsed();
	delete[] arr;
	List_Clear(head);
	delete head;
	return 0;
}
