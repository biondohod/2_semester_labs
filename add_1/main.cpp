#include <iostream>
const int N = 5;

float Mean(float* arr, int numLength, float* mean) {
    if (numLength > 0) {
        *mean += arr[N - numLength];
        numLength--;
        Mean(arr, numLength, mean);
    }

    return (*mean / N);
}

int main()
{
    float* arr = new float[N];
    int numLength = N;
    float mean = 0;
    std::cout << "input number sequence" << std::endl;
    for (int i = 0; i < N; i++)
        std::cin >> arr[i];

    std::cout << "mean: " << Mean(arr, numLength, &mean) << '\n';

    delete[] arr;
    return 0;
}