#include <iostream>
#include <fstream>
#include <string>


struct Binar_Tree
{
    int data;      
    
    Binar_Tree* left;
    Binar_Tree* right;
    Binar_Tree* parent;
};

Binar_Tree* Tree_Make(int data, Binar_Tree* p)
{

    Binar_Tree* x = new Binar_Tree;          
    x->data = data;
    x->left = nullptr;
    x->right = nullptr;
    x->parent = p;
    return x;
}

void Tree_Add(int data, Binar_Tree*& root)
{
    if (root == nullptr) {
        root = Tree_Make(data, nullptr);
        return;
    }
    Binar_Tree* y = root;
    while ((data < y->data && y->left != nullptr) || (data > y->data && y->right != nullptr))
        if (data < y->data)
            y = y->left;
        else
            y = y->right;
    if (data == y->data)
        return;
    Binar_Tree* u = Tree_Make(data, y);
    if (data < y->data)
        y->left = u;
    else
        y->right = u;
}

Binar_Tree* Tree_Search(int data, Binar_Tree* v, int& count)
{
    if (v == nullptr) {
        count = -1;
        return v;
    }
   
    if (v->data == data)
        return v;
    if (data < v->data) {
        count++;
        return Tree_Search(data, v->left, count);
    }
    else {
        count++;
        return Tree_Search(data, v->right, count);
    }
}


void Tree_Delete(int data, Binar_Tree*& root, int& count)
{
    Binar_Tree* u = Tree_Search(data, root, count);
    if (u == nullptr)
        return;
    if (u->left == nullptr && u->right == nullptr && u == root)
    {
        delete root;
        root = nullptr;
        return;
    }

    if (u->left == nullptr && u->right != nullptr && u == root) 
    {
        Binar_Tree* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }

    if (u->left != nullptr && u->right == nullptr && u == root) 
    {
        Binar_Tree* t = u->left;
        while (t->right != nullptr)
            t = t->right;
        u->data = t->data;
        u = t;
    }
    if (u->left != nullptr && u->right != nullptr)
    {
        Binar_Tree* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }
    Binar_Tree* t;
    if (u->left == nullptr)
        t = u->right;
    else
        t = u->left;
    if (u->parent->left == u)
        u->parent->left = t;
    else
        u->parent->right = t;
    if (t != nullptr)
        t->parent = u->parent;
    delete u;
}

void Tree_Clear(Binar_Tree*& v)
{
    if (v == nullptr)
        return;
    Tree_Clear(v->left);
    Tree_Clear(v->right);
    delete v;
    v = nullptr;
}

int main()
{
    Binar_Tree* root = nullptr;
    int count = 0;
    std::string a;
    std::cin >> a;
    while (a != "E")
    {
        if (a[0] == '+') {
            a.erase(0, 1);
            int x = std::stoi(a);
            Tree_Add(x, root);
        }
        else if (a[0] == '-') {
            a.erase(0, 1);
            int x = std::stoi(a);
            Tree_Delete(x, root, count);
        }
        else if (a[0] == '?') {
            a.erase(0, 1);
            int x = std::stoi(a);
            count = 1;
            Tree_Search(x, root, count);
            if (count == -1)
                std::cout << "n" << std::endl;
            else
                std::cout << count << std::endl;
            count = 1;
        }
        else if (a[0] == 'E') {
            break;
        }
        std::cin >> a;
    }


    Tree_Clear(root);



    return 0;
}

