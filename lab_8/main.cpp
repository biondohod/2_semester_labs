#include <iostream>
#include <inc/matrix.hpp>





int main()
{
	setlocale(LC_ALL, "rus");
	std::cout << "Тест матриц 2 на 2" << std::endl << "1. Тест работы обратной матрицы" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		try
		{
			std::cout << A.Reverse() << std::endl;
			std::cout << "After inv in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "2. Тест работы транспонирования матрицы" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		try
		{
			std::cout << A.Transp() << std::endl;
			std::cout << "After transp in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "3. Тест работы нахождения определителя матрицы" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		try
		{
			std::cout << A.Det() << std::endl;
			std::cout << "After det in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "Тест матриц 3 на 3" << std::endl << "1. Тест работы обратной матрицы" << std::endl;
	{
		Mat33d A({ {
			 {1,2,3},
			 {4,5,6},
			 {7,8,9}
		} });

		try
		{
			std::cout << A.Reverse() << std::endl;
			std::cout << "After inv in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "2. Тест работы транспонирования матрицы" << std::endl;
	{
		Mat33d A({ {
			 {1,2,3},
			 {4,5,6},
			 {7,8,9}
		} });

		try
		{
			std::cout << A.Transp() << std::endl;
			std::cout << "After transp in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "3. Тест работы нахождения определителя матрицы" << std::endl;
	{
		Mat33d A({ {
			 {1,2,3},
			 {4,5,6},
			 {7,8,9}
		} });

		try
		{
			std::cout << A.Det() << std::endl;
			std::cout << "After det in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	return 0;
}
