#pragma once
#include <iostream>

//#define DEBUG 
		template<typename T, int I, int J>
		struct MasWrapper
		{
			T mas[I][J];
		};

		template<typename T, int I, int J>
		class Matrix
		{
		public:
			// Конструктор
			Matrix()
			{
	#ifdef DEBUG
				std::cout << "Конструктор нулей работает!" << std::endl;
	#endif
				m_i = I;
				m_j = J;
				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < m_j; j++)
						m_matrix[i][j] = 0;
			}

			// Конструктор
			Matrix(const MasWrapper<T, I, J>& mas)
			{
	#ifdef DEBUG
				std::cout << "Конструктор работает!" << std::endl;
	#endif
				m_i = I;
				m_j = J;
				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < m_j; j++)
						m_matrix[i][j] = mas.mas[i][j];
			}

			// Конструктор копирования
			Matrix(const Matrix<T, I, J>& mat)
			{
	#ifdef DEBUG
				std::cout << "Копирующий конструктор" << std::endl;
	#endif

				m_i = mat.m_i;
				m_j = mat.m_j;

				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < m_j; j++)
						m_matrix[i][j] = mat.m_matrix[i][j];
			}

			int getN() const { return m_i; }
			int getM() const { return m_j; }
			int get(int i, int j) const { return m_matrix[i][j]; }
			void set(int i, int j, T data) { m_matrix[i][j] = data; }

			// Присваивание
			template<typename T, int I, int J>
			Matrix<T, I, J>& operator=(const Matrix<T, I, J>& mat)
			{
	#ifdef DEBUG
				std::cout << "Присваивание" << std::endl;
	#endif

				m_i = mat.getN();
				m_j = mat.getM();

				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < m_j; j++)
						m_matrix[i][j] = mat.get(i, j);

				return *this;
			}

			// Оператор сложения
			template<typename T, int I, int J>
			Matrix<T, I, J> operator+(const Matrix<T, I, J>& mat)
			{
	#ifdef DEBUG
				std::cout << "Сложение" << std::endl;
	#endif
				Matrix<T, I, J> tmp;
				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < m_j; j++)
						tmp.m_matrix[i][j] = m_matrix[i][j] + mat.m_matrix[i][j];
				return tmp;
			}

			// Оператор умножения
			template<typename T, int I, int J>
			Matrix<T, I, J> operator*(const Matrix<T, I, J>& mat)
			{
	#ifdef DEBUG
				std::cout << "Умножение" << std::endl;
	#endif
				Matrix<T, I, J> tmp;

				for (int i = 0; i < m_i; i++)
					for (int j = 0; j < mat.getM(); j++)
					{
						int sum = 0;
						for (int k = 0; k < m_j; k++)
							sum += m_matrix[i][k] * mat.get(i, j);
						tmp.set(i, j, sum);
					}

				return tmp;
			}

			// Деструктор
			~Matrix()
			{
	#ifdef DEBUG
				std::cout << "Деструктор работает!" << std::endl;
	#endif
			}
			//опрелелитель
			int Matrix::Det()
			{
				if ((m_i == 2) && (m_j == 2))
				{
					return (m_matrix[0][0] * m_matrix[1][1] - m_matrix[1][0] * m_matrix[0][1]);
				}
				else if ((m_i == 3) && (m_j == 3))
				{
					return (m_matrix[0][0] * (m_matrix[1][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[1][2]) - (m_matrix[0][1] * (m_matrix[1][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[1][2])) + (m_matrix[0][2] * (m_matrix[1][0] * m_matrix[2][1] - m_matrix[1][1] * m_matrix[2][0])));
				}
				else
				{
					std::cout << "Ошибка! Невозможно вычислить определитель" << std::endl;
				}
			}

			//транспонирование
			Matrix Matrix::Transp()
			{
				Matrix<T, I, J> Y;
				for (int i = 0; i < m_i; i++)
				{
					for (int j = 0; j < m_j; j++)
					{
						Y.m_matrix[i][j] = m_matrix[j][i];
					}
				}
				return Y;
			}
			//обратная
			Matrix Matrix::Reverse()
			{
				Matrix<T, I, J> Y;
				for (int i = 0; i < m_i; i++)
				{
					for (int j = 0; j < m_j; j++)
					{
						Y.m_matrix[i][j] = 0;
					}
				}
				if ((m_i == 2 && m_j == 2) || (m_i == 3 && m_j == 3))
				{
					int det = Det();
					if (det == 0) {
						std::cout << "Ошибка! Определитель равен нулю, обратной не существует" << std::endl;
						return Y;
					}
					if (m_i == 2) {
						Y.m_matrix[0][0] = m_matrix[1][1] / det;
						Y.m_matrix[0][1] = -m_matrix[0][1] / det;
						Y.m_matrix[1][0] = -m_matrix[1][0] / det;
						Y.m_matrix[1][1] = m_matrix[0][0] / det;
						return Y;
					}
					if (m_i == 3) {
						Y.m_matrix[0][0] = (m_matrix[1][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[1][2]) / det;
						Y.m_matrix[0][1] = -(m_matrix[0][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[0][2]) / det;
						Y.m_matrix[0][2] = (m_matrix[0][1] * m_matrix[1][2] - m_matrix[1][1] * m_matrix[0][2]) / det;
						Y.m_matrix[1][0] = -(m_matrix[1][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[1][2]) / det;
						Y.m_matrix[1][1] = (m_matrix[0][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[0][2]) / det;
						Y.m_matrix[1][2] = -(m_matrix[0][0] * m_matrix[1][2] - m_matrix[1][0] * m_matrix[0][2]) / det;
						Y.m_matrix[2][0] = (m_matrix[1][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[1][1]) / det;
						Y.m_matrix[2][1] = -(m_matrix[0][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[0][1]) / det;
						Y.m_matrix[2][2] = (m_matrix[0][0] * m_matrix[1][1] - m_matrix[1][0] * m_matrix[0][1]) / det;
						return Y;
					}
				}
				else
				{
					std::cout << "Ошибка! Не удается найти обратную матрицу" << std::endl;
				}
			}

			template<typename T, int I, int J>
			friend std::istream& operator>>(std::istream& os, Matrix<T, I, J>& mat);
			template<typename T, int I, int J>
			friend std::ostream& operator<<(std::ostream& os, const Matrix<T, I, J>& mat);

		private:
			int m_i, m_j;
			T m_matrix[I][J];
		};
		// Перегрузка ввода
		template<typename T, int I, int J>
		std::istream& operator>>(std::istream& in, Matrix<T, I, J>& mat)
		{
			for (int i = 0; i < mat.m_i; i++)
				for (int j = 0; j < mat.m_j; j++)
					in >> mat.m_matrix[i][j];
			return in;
		}

		// Перегрузка вывода
		template<typename T, int I, int J>
		std::ostream& operator<<(std::ostream& out, const Matrix<T, I, J>& mat)
		{
			out << "Matrix " << mat.m_i << "x" << mat.m_j << std::endl;
			for (int i = 0; i < mat.m_i; i++) {
				for (int j = 0; j < mat.m_j; j++)
					out << mat.m_matrix[i][j] << " ";
				out << std::endl;
			}
			return out;
		}

		using Vec2i = Matrix<int, 2, 1>;
		using Vec2d = Matrix<double, 2, 1>;
		using Mat22i = Matrix<int, 2, 2>;
		using Mat22d = Matrix<double, 2, 2>;
		using Mat33i = Matrix<int, 3, 3>;
		using Mat33d = Matrix<double, 3, 3>;

