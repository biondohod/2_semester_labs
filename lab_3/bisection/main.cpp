#include <iostream>
#pragma comment(linker, "/STACK:40000000")
double Func(double x)
{
	return sin((2 * x) + 3);
}
double Bisection(double left, double right, double epsilon)
{
	while (right - left > epsilon)
	{
		if (Func(left) * Func((right + left) / 2) == 0)
			break;
		else if (Func(left) * Func((right + left) / 2) > 0)
			left = (right + left) / 2;
		else
			right = (right + left) / 2;
	}
	return (right + left) / 2;
}
int main()
{
	std::cout << Bisection(0, 1, pow(10, -4));
	return 0;
}


