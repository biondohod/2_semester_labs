﻿#include <iostream>
#include <fstream>
#include "inc/FunctionsHead.hpp"
#include "inc/mathutils/matrix.hpp"
#pragma comment(linker, "/STACK:40000000")


int main() {
	BMP bmp_Image;
	bmp_Image.Read();
	bmp_Image.Pixels();
	bmp_Image.DarkFilter();
	bmp_Image.Write();
	bmp_Image.Clean();
	return 0;
}

