#include <iostream>
#include <chrono>
const int N = 100000;
const int M = 10000;
class Timer
{
private:
	// ���������� ����� ������������ ��� �������� ������� � ��������� �����
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

int BinarSearch(int left, int right, int* arr, int key)
{
	if ((key < arr[0]) or (key > arr[N - 1]))
	{
		std::cout << "Error! This number not find" << std::endl;
		std::cout << arr[0] << " " << arr[-1] << std::endl;
		return -1;
	}
	int middle = (left + right) / 2;
	if (arr[middle] == key)
		return middle;
	if (arr[middle] > key)
		BinarSearch(left, middle, arr, key);
	if (arr[middle] < key)
		BinarSearch(middle, right, arr, key);
}
void QuickSort(int left, int right, int* arr)
{
	if (left < right)
	{
		//int medium = rand() % N;
		int medium = (left + right) / 2;
		int k = arr[medium];
		do
		{
			while (arr[left] < k)
				left++;
			while (arr[right] > k)
				right--;
			if (left <= right)
			{
				std::swap(arr[left], arr[right]);
				left++;
				right--;
			}
		} while (left < right);
		QuickSort(left, right, arr);
		QuickSort(left, right, arr);
	}
}

int main()
{
	Timer t;
	int left = 0;
	int right = N - 1;
	int key;
	int arr[N];
	for (int i = 0; i < N; i++)
		arr[i] = i;
	/*for (int i = 0; i < M; i++)
	{
		key = rand();
		BinarSearch(left, right, arr, key);
	}*/
	for (int i = 0; i < M; i++)
	{
		key = rand();
		for (int j = 0; j < N; j++)
		{
			if (arr[j] == key)
				break;
		}
	}
	std::cout << "Time elapsed: " << t.elapsed();
	return 0;
}


