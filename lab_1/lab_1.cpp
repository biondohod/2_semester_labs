﻿#include <iostream>
#include <chrono>
class Timer
{
private:
	// Псевдонимы типов используются для удобного доступа к вложенным типам
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};
int main()
{
	Timer t;
	int num = 10000;
	/*std::cout << "Input number : ";
	std::cin >> num;*/
	std::cout << std::endl;
	unsigned long long factorial = 1;
	for (int i = 1; i <= num; i++)
	{
		factorial = factorial * i;
	}
	std::cout << "Time elapsed: " << t.elapsed() << std::endl;
	std::cout << "Factorial= " << factorial;
	return 0;
}
