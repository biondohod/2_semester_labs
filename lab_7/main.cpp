#include <iostream>

class Matrix
{
public:
	//конструктор
	Matrix::Matrix(int n, int m)
	{
		m_n = n;
		m_m = m;
		m_matrix = new int* [m_n];
		for (int i = 0; i < m_n; i++)
			m_matrix[i] = new int[m_m];
	}
	// конструктор копирования
	Matrix::Matrix(const Matrix& matrix)
	{
		m_n = matrix.m_n;
		m_m = matrix.m_m;

		m_matrix = new int* [m_n];
		for (int i = 0; i < m_n; i++)
			m_matrix[i] = new int[m_m];

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				m_matrix[i][j] = matrix.m_matrix[i][j];
	}

	Matrix& Matrix::operator=(const Matrix& matrix) = delete;
	// оператор сложения матриц
	Matrix Matrix::operator+=(const Matrix& matrix) {
		Matrix Y(3, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				Y.m_matrix[i][j] = m_matrix[i][j] + matrix.m_matrix[i][j];
		return Y;
	}

	// оператор умножения
	Matrix Matrix::operator*(const Matrix& mat) {
		Matrix Y(3, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				Y.m_matrix[i][j] = m_matrix[i][j] + mat.m_matrix[i][j];
		return Y;
	}

	// деструктор
	Matrix::~Matrix()
	{
		for (int i = 0; i < m_n; i++)
			delete[] m_matrix[i];
		delete m_matrix;
	}


	//опрелелитель
	int Matrix::Det()
	{
		if ((m_n == 2) && (m_m == 2))
		{
			return (m_matrix[0][0] * m_matrix[1][1] - m_matrix[1][0] * m_matrix[0][1]);
		}
		else if ((m_n == 3) && (m_m == 3))
		{
			return (m_matrix[0][0] * (m_matrix[1][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[1][2]) - (m_matrix[0][1] * (m_matrix[1][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[1][2])) + (m_matrix[0][2] * (m_matrix[1][0] * m_matrix[2][1] - m_matrix[1][1] * m_matrix[2][0])));
		}
		else
		{
			std::cout << "Ошибка! Невозможно вычислить определитель" << std::endl;
		}
	}

	//транспонирование
	Matrix Matrix::Transp()
	{
		Matrix Y(m_n, m_m);
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				Y.m_matrix[i][j] = m_matrix[j][i];
			}
		}
		return Y;
	}
	//обратная
	Matrix Matrix::Reverse()
	{
		Matrix Y(m_n, m_m);
		for (int i = 0; i < m_n; i++)
		{
			for (int j = 0; j < m_m; j++)
			{
				Y.m_matrix[i][j] = 0;
			}
		}
		if ((m_n == 2 && m_m == 2) || (m_n == 3 && m_m == 3))
		{
			int det = Det();
			if (det == 0) {
				std::cout << "Ошибка! Определитель равен нулю, обратной не существует" << std::endl;
				return Y;
			}
			if (m_n == 2) {
				Y.m_matrix[0][0] = m_matrix[1][1] / det;
				Y.m_matrix[0][1] = -m_matrix[0][1] / det;
				Y.m_matrix[1][0] = -m_matrix[1][0] / det;
				Y.m_matrix[1][1] = m_matrix[0][0] / det;
				return Y;
			}
			if (m_n == 3) {
				Y.m_matrix[0][0] = (m_matrix[1][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[1][2]) / det;
				Y.m_matrix[0][1] = -(m_matrix[0][1] * m_matrix[2][2] - m_matrix[2][1] * m_matrix[0][2]) / det;
				Y.m_matrix[0][2] = (m_matrix[0][1] * m_matrix[1][2] - m_matrix[1][1] * m_matrix[0][2]) / det;
				Y.m_matrix[1][0] = -(m_matrix[1][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[1][2]) / det;
				Y.m_matrix[1][1] = (m_matrix[0][0] * m_matrix[2][2] - m_matrix[2][0] * m_matrix[0][2]) / det;
				Y.m_matrix[1][2] = -(m_matrix[0][0] * m_matrix[1][2] - m_matrix[1][0] * m_matrix[0][2]) / det;
				Y.m_matrix[2][0] = (m_matrix[1][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[1][1]) / det;
				Y.m_matrix[2][1] = -(m_matrix[0][0] * m_matrix[2][1] - m_matrix[2][0] * m_matrix[0][1]) / det;
				Y.m_matrix[2][2] = (m_matrix[0][0] * m_matrix[1][1] - m_matrix[1][0] * m_matrix[0][1]) / det;
				return Y;
			}
		}
		else
		{
			std::cout << "Ошибка! Не удается найти обратную матрицу" << std::endl;
		}
	}
	friend std::istream& operator>>(std::istream& os, Matrix& matrix);
	friend std::ostream& operator<<(std::ostream& os, const Matrix& matrix);

private:
	int m_n, m_m;
	int n;
	int** m_matrix;

};

// ввод
std::istream& operator>>(std::istream& in, Matrix& matrix) {
	for (int i = 0; i < matrix.m_n; i++)
		for (int j = 0; j < matrix.m_m; j++)
			in >> matrix.m_matrix[i][j];
	return in;
}

// вывод
std::ostream& operator<<(std::ostream& out, const Matrix& matrix)
{
	for (int i = 0; i < matrix.m_n; i++) {
		for (int j = 0; j < matrix.m_m; j++)
			out << matrix.m_matrix[i][j] << " ";
		out << std::endl;
	}
	return out;
}


int main()
{
	
	setlocale(LC_ALL, "rus");
	std::cout << "Программа позволяет посчитать определитель матрицы, транспонировать ее и найти обратную к ней. К сожалению, действия возможно только для матриц типа 2х2 и 3х3" << std::endl;
	std::cout << "Введите 1, если хотите обработать матрицу типа 2х2 или введите 2 для действий с матрицей 3х3" << std::endl;
	int a;
	std::cin >> a;
	if (a == 1) {
		Matrix A(2, 2);
		std::cout << "Введите вашу матрицу типа 2x2" << std::endl;
		std::cin >> A;
		std::cout << "Определитель=" << A.Det() << std::endl;
		std::cout << "Транспонированная матрица:" << std::endl << A.Transp() << std::endl;
		std::cout << "Обратная матрица:" << std::endl << A.Reverse() << std::endl;
	}
	else if (a == 2) {
		Matrix B(3, 3);
		std::cout << "Введите вашу матрицу типа 3x3" << std::endl;
		std::cin >> B;
		std::cout << "Определитель=" << B.Det() << std::endl;
		std::cout << "Транспонированная матрица:" << std::endl << B.Transp() << std::endl;
		std::cout << "Обратная матрица:" << std::endl << B.Reverse() << std::endl;
	}
	else {
		std::cout << "Введено неверное значение" << std::endl;
	}
	return 0;
}
