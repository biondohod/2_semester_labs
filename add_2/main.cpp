#include <iostream>
#include <vector>
const int N = 50;

void bubbleSort(int n, std::vector<int>& index, int* arr) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[index[j]] > arr[index[j + 1]]) {
                int buff = arr[index[j]];
                arr[index[j]] = arr[index[j + 1]];
                arr[index[j + 1]] = buff;
            }
        }
    }
}

void schellSort(int n, int* arr, std::vector<int>& ciura) {
    int step = ciura[ciura.size() - 1];
    for (int i = 0; i < n; i++) {
        std::vector<int> index;
        for (int j = 0; j < (((n - i - 1) / step) + 1); j++) {
            index.push_back(i + step * j);
        }
        bubbleSort(index.size(), index, arr);
    }
    if (step != 1) {
        ciura.erase(ciura.begin() + ciura.size() - 1);
        schellSort(n, arr, ciura);
    }
}

int main()
{
    int* arr = new int[N];
    for (int i = 0; i < N; i++) {
        arr[i] = rand();
        std::cout << arr[i] << '\n';
    }
    std::cout << "------------" << '\n';

    std::vector<int> ciura = { 1, 4, 10, 23, 57, 132, 301, 701, 1750 };
    schellSort(N, arr, ciura);

    std::cout << "------------" << '\n';
    for (int i = 0; i < N; i++)
        std::cout << "Index " << i << ": " << arr[i] << '\n';

    delete[] arr;
    return 0;
}